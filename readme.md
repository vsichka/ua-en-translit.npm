# ua-en-translit
 
![][logo]

[![NPM Downloads][downloads-image]][downloads-url]
![GitLab Pipeline Status][gitlab-pipeline-image]

## Description
 
Ukrainian to English transliteration using official rules - you can see it at [official web-page](http://zakon3.rada.gov.ua/laws/show/55-2010-%D0%BF) of Verkhovna Rada of Ukraine.

## Installation
 
```sh
npm install ua-en-translit
```

## Initialization

```js
// Import.
const translit = require('ua-en-translit');
```

## Using

```js
// Translit.
const enName = translit('Тест Тестович');
// Value of "enName": "Test Testovych".
```

## License

The MIT License (MIT)

Copyright © 2016 - 2023 Volodymyr Sichka

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[logo]: https://gitlab.com/vsichka/ua-en-translit.npm/raw/master/logo/logo.png
[downloads-image]: https://img.shields.io/npm/dm/ua-en-translit.svg
[downloads-url]: https://npmjs.org/package/ua-en-translit
[gitlab-pipeline-image]: https://gitlab.com/vsichka/ua-en-translit.npm/badges/master/pipeline.svg?ignore_skipped=true
