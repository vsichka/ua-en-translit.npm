// Import.
const assert = require('assert');
const translit = require('../index');

// Constants.
const TEST_DATA_LIST = [
  { ua: 'Володимир Січка', en: 'Volodymyr Sichka' },
  { ua: 'Марія', en: 'Mariia' },
  { ua: 'Єнакієво', en: 'Yenakiievo' },
  { ua: 'Мар\'ян', en: 'Marian' },
  { ua: 'Їхній', en: 'Yikhnii' },
  { ua: 'Йорж', en: 'Yorzh' },
  { ua: 'Щука', en: 'Shchuka' },
  { ua: 'Юрію', en: 'Yuriiu' },
  { ua: 'Ярослав', en: 'Yaroslav' },
  { ua: 'Згода', en: 'Zghoda' },
  { ua: 'Батько', en: 'Batko' },
  { ua: 'Тест Тестович', en: 'Test Testovych' },
  { ua: 'Текст для трансліту.', en: 'Tekst dlia translitu.' },
  { ua: 'Текст з цифрами та спецсимволами 1234567890 !@#$%^&*();\',./?|№', en: 'Tekst z tsyframy ta spetssymvolamy 1234567890 !@#$%^&*();\',./?|№' }
];

/**
 * Check sync.
 */
describe('Check sync.', () => {
  it('Should be equals with precalculated results.', () => {
    for (const testData of TEST_DATA_LIST) {
      const { ua, en } = testData;
      const returnedEn = translit(ua);
      assert.equal(returnedEn, en);
    }
  });
});

/**
 * Check async.
 */
describe('Check async.', () => {
  it('Should be equals with precalculated results.', async () => {
    for (const testData of TEST_DATA_LIST) {
      const { ua, en } = testData;
      const returnedEn = await translit.async(ua);
      assert.equal(returnedEn, en);
    }
  });
});
